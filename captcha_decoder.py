from PIL import Image, ImageFilter
import sys
import string
import hashlib
import time
import math
import os
from cv2 import rotate, threshold
from numpy import character
import numpy
from skimage.metrics import structural_similarity
from skimage.transform import resize
import cv2

mask_letters = string.ascii_uppercase  + string.digits
alphabet_characters= []

def clear_folder(foldername):
    folder_files = os.listdir(foldername)
    for filename in folder_files:
        os.remove(os.path.join(foldername,filename))

def orb_sim(img1, img2):
  # SIFT is no longer available in cv2 so using ORB
  orb = cv2.ORB_create()

  # detect keypoints and descriptors
  kp_a, desc_a = orb.detectAndCompute(img1, None)
  kp_b, desc_b = orb.detectAndCompute(img2, None)

  # define the bruteforce matcher object
  bf = cv2.BFMatcher(cv2.NORM_HAMMING, crossCheck=True)
    
  #perform matches. 
  matches = bf.match(desc_a, desc_b)
  #Look for similar regions with distance < 50. Goes from 0 to 100 so pick a number between.
  similar_regions = [i for i in matches if i.distance < 50]  
  if len(matches) == 0:
    return 0.0
  return len(similar_regions) / len(matches)


#Needs images to be same dimensions
def structural_sim(img1, img2):
    sim, diff = structural_similarity(img1, img2, full=True)
    return sim

def load_letters_map(letter_map_filename):
    global alphabet_characters
    clear_folder("alphabet_characters")
    
    letter_image = Image.open(letter_map_filename)
    inletter = False
    foundletter = False
    start = 0
    end = 0

    letters = []

    for y in range(letter_image.size[0]):
        for x in range(letter_image.size[1]):
            pix = letter_image.getpixel((y,x))[0]
            # if pix > 80: best precision
            # if pix > 180: full amount 
            if pix > 80:
                inletter = True

        if foundletter == False and inletter == True:
            foundletter = True
            start = y

        if foundletter == True and inletter == False:
            foundletter = False
            end = y
            letters.append((start,end))
        inletter=False
    count = 0
    for letter in letters:
        m = hashlib.md5()
        found_letter_image = letter_image.crop(( letter[0] , 0, letter[1],letter_image.size[1] ))
        m.update(("{}{}".format(time.time(),count)).encode('utf-8'))
        found_letter_image.save("alphabet_characters/{}.png".format(mask_letters[count]))
        count += 1
        alphabet_characters.append(found_letter_image)
    
    
    
        
def buildvector(im):
    d1 = {}
    count = 0
    for i in im.getdata():
        d1[count] = i
        count += 1
    return d1

def decoder(
        im,
        threshold=180,
        mask="letters.bmp",
        alphabet=mask_letters):

    img = Image.open(im)
    img = img.convert("RGB")
    box = (0, 0, 83, 21)
    img = img.crop(box)
    pixdata = img.load()
    clear_folder("tmp_letters")
    
    # open the mask
    letters = Image.open(mask)
    ledata = letters.load()

    # Clean the background noise, if color != white, then set to black.
    for y in range(img.size[1]):
        for x in range(img.size[0]):
            if (pixdata[x, y][0] > threshold) \
                    and (pixdata[x, y][1] > threshold) \
                    and (pixdata[x, y][2] > threshold):

                pixdata[x, y] = (255, 255, 255, 255)
            else:
                pixdata[x, y] = (0, 0, 0, 255)
    
    img2 = Image.new("P",img.size,255)
    for width in range(img.width):
        for height in range(img.height):
            img2.putpixel((width,height),pixdata[width,height])
    img2.save("processed.png")
    
    # Clean the pixel noise left from the background noise removal
    img3 = Image.new("P",img.size,255)
    img3 = img2
    for width in range(img.width):
        for height in range(img.height):
            pixel = img3.getpixel((width,height))
            lonely_pixel = True
            if (pixel == 1):
                for x in range(3):
                    for y in range(3):
                        # check if the verified pixel is in the image
                        if(width-1+x<img.width and height-1+y<img.height):
                            # check that is not the same pixel
                            if( x != 1 and y != 1):
                                if(img3.getpixel((width-1+x,height-1+y)) == 1):
                                    lonely_pixel = False
            if lonely_pixel:
                img3.putpixel((width,height),0)
                                                            
    img3.save("filter.png")
    # now we need to port img3 into img
    img = img3
    
    # separate each letter
    inletter = False
    foundletter = False
    start = 0
    end = 0

    found_letters = []
    
    characters_to_guess = []

    for y in range(img.size[0]):
        for x in range(img.size[1]):
            pix = img.getpixel((y,x))
            # if pix != 255:
            if pix == 1:
                inletter = True

        if foundletter == False and inletter == True:
            foundletter = True
            start = y

        if foundletter == True and inletter == False:
            foundletter = False
            end = y
            found_letters.append((start,end))
        inletter=False
    
    count = 0
    for letter in found_letters:
        m = hashlib.md5()
        im3 = img.crop(( letter[0] , 0, letter[1],img.size[1] ))
        m.update(("{}{}".format(time.time(),count)).encode('utf-8'))
        # print(m.hexdigest())
        path_to_save_image = "tmp_letters/{}.png".format(m.hexdigest())
        characters_to_guess.append(path_to_save_image)
        im3.save(path_to_save_image)
        count += 1
            
    answer = ""
    for character_to_guess in characters_to_guess:
        image1 = cv2.imread(character_to_guess, 0)
        alphabet_characters = os.listdir("alphabet_characters")
        print(character_to_guess)
        biggest_ssim = 0
        best_ssim_guess = None
        
        for character in alphabet_characters:
            image2 = cv2.imread(os.path.join("alphabet_characters",character),0)
            # print(character)
            for rotation in range(0,360,15):
                #rotate image at rotation
                (image_height, image_width) = image1.shape[:2]
                (center_X, center_Y) = (image_width // 2, image_height // 2)
                rotation_engine = cv2.getRotationMatrix2D((center_X, center_Y), rotation, 1.0)
                rotated_image = cv2.warpAffine(image1, rotation_engine, (image_width, image_height))
                
                orb_similarity = orb_sim(rotated_image,image2)
                if orb_similarity == 0:
                    
                    # if (rotated_image.shape[0] >= 7 and rotated_image.shape[1] >= 7):
                    #     if (image2.shape[0] >= 7 and image2.shape[1] >= 7):
                    image2_resized = image2
                    if (image2_resized.shape[0] < 7 or image2_resized.shape[1] < 7):
                        if (rotated_image.shape[0] < 7 or rotated_image.shape[1] < 7):
                            print("upscale both")
                        image2_resized = resize(image2_resized,(rotated_image.shape[0],rotated_image.shape[1]), anti_aliasing=True, preserve_range=True)
                        
                    # print(character)
                    # print("before:")
                    # print("1st:",image2_resized.shape)
                    # print("2nd:",rotated_image.shape)
                    # print("after:")
                        
                    image1_resized = resize(rotated_image,(image2_resized.shape[0],image2_resized.shape[1]), anti_aliasing=True, preserve_range=True)
                    ssim = structural_sim(image2_resized, image1_resized)
                    # image2_resized = resize(image2, (rotated_image.shape[0], rotated_image.shape[1]), anti_aliasing=True, preserve_range=True)
                    # ssim = structural_sim(rotated_image, image2_resized)
                    if (ssim > biggest_ssim):
                        biggest_ssim = ssim
                        best_ssim_guess = character
        print(best_ssim_guess,biggest_ssim)
        answer+=best_ssim_guess.split('.')[0]
    return answer

if __name__ == '__main__':
    load_letters_map("letters.bmp")
    print(decoder(sys.argv[1]))
